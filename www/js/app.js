// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic', 'firebase']);
var fb = null;

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    OfflineFirebase.restore();
    fb = new OfflineFirebase("https://flickering-torch-4284.firebaseio.com/");
  });
}).config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
          url: '/login',
          templateUrl: 'templates/login.html',
          controller: 'LoginController'
      })
      .state('todo', {
          url: '/todo',
          templateUrl: 'templates/todo.html',
          controller: 'TodoController'
      });
    $urlRouterProvider.otherwise('/login');
}]);

app.controller("LoginController", function($scope, $firebaseAuth, $location){
  
  $scope.login = function(username, password){
    var fbAuth = $firebaseAuth(fb);

    fbAuth.$authWithPassword({
      email : username,
      password : password
    }).then(function(authData){
      $location.path("/todo");
    }).catch(function(error){
      alert("ERROR " + error);
    });
  }

  $scope.register = function(username, password){
    var fbAuth = $firebaseAuth(fb);
    fbAuth.$createUser({email: username, password: password}).then(function(){
      return fbAuth.$authWithPassword({
        email: username,
        password: password
      });
    }).then(function(authData){
      $location.path("/todo");
    }).catch(function(error){
      alert("ERROR " + error);
    });
  }

});

app.controller("TodoController", function($scope, $firebaseObject, $ionicPopup){

  $scope.list = function(){
    fbAuth = fb.getAuth();
    if (fbAuth) {
      var syncObject = $firebaseObject(fb.child("users/"+ fbAuth.uid));
      syncObject.$bindTo($scope, "data");
    }

    fb.on('value', function(snapshot) {
        console.log(snapshot.val());
    }, undefined, undefined, true);
  }

  $scope.create = function(){
    $ionicPopup.prompt({
      title: "Informe um novo item a fazer",
      inputType: "text"
    })
    .then(function(result){
      if(result != "") {
        if ($scope.data.hasOwnProperty("todos") !== true) {
          $scope.data.todos = [];
        }
        $scope.data.todos.push({title : result});
      } else {
        console.log("Ação não completada");
      }
    });
  }

  $scope.sync = function(){
    fb.on('value', function(snapshot) {
        console.log(snapshot.val());
    }, undefined, undefined, true);
  }

});
